
Link Notion : [Agile Foundation](https://unexpected-jewel-25d.notion.site/Agile-Foundation-0029492e92e740169747cce577ce9e17)

*Terjemahan*:

1. Epic: Sebuah hotel perlu membuat pemesanan kamar dalam aplikasi website selama liburan, seperti liburan natal (Rata-rata tamu: 110 tamu/hari)
2. User Stories:
- Sediakan informasi kamar yang sesuai dengan keinginan tamu
- Periksa kamar yang tersedia untuk tamu
- Harga diskon untuk tamu langganan setia
3. Tasks:
- Buat informasi kamar ke dalam website (Wawancara mengenai kamar hotel mereka)
- Menjumlahkan seluruh kamar hotel yang tersedia untuk pemesanan
- Berikan harga diskon ketika proses transaksi dengan tamu langganan setia